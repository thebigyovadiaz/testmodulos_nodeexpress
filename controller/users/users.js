var express = require('express');
var app = module.exports = express();

app.set('views', __dirname + '/views');

app.get('/', function(req, res){
	res.render('index', {title: 'Hola desde el controlador de users'});
});