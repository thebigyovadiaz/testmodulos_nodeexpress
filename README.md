# Testeando Módulos con NodeJS Express

	 Aplicación desarrollada con NodeJs y Express para testear módulos contenidos en controladores. 

## Instalación

	Luego de realizar la descarga ó clonado del repositorio, se debe:
		* Posicionarse en la carpeta y aplicar por consola (Terminal) -> ´npm install´
		* Ejecutar servidor aplicando por consola (Terminal) -> ´node ./bin/www´
		* Abrir navegador y colocar la url -> ´localhost:3030´

		* Probar las siguientes rutas:
			- Módulo Home -> ´localhost:3030/´
			- Módulo Users -> ´localhost:3030/users´
		
